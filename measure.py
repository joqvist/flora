import serial
import time
import datetime

with serial.Serial('/dev/ttyACM0') as ser:
    print("connected to %s" % ser.name)
    time.sleep(2) # Wait for Arduino to reboot.
    print('Measuring %s' % datetime.datetime.now().ctime())
    ser.write(b'm')
    ser.flush()
    m1 = False
    m2 = False
    v1 = ''
    v2 = ''
    while not (m1 and m2):
        line = ser.readline()
        if line.startswith('m1: '):
            m1 = True
            v1 = line[4:].strip()
        elif line.startswith('m2: '):
            m2 = True
            v2 = line[4:].strip()
    timestamp = int(time.time())
    print('%d, %s, %s' % (timestamp, v1, v2))

