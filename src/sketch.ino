#define MOTORPIN (9)
#define PROBEPIN (12)
#define MOIST1 (A1)
#define MOIST2 (A2)
#define LEDPIN (13)
#define WARMUP (700L)
#define PERIOD (30L * 60L * 1000L - WARMUP)
#define WATERTIME (5000)

void setup()
{
	pinMode(MOTORPIN, OUTPUT);
	pinMode(PROBEPIN, OUTPUT);
	pinMode(LEDPIN, OUTPUT);

	Serial.begin(9600);
	while (!Serial) {
		// Waiting for serial port to connect.
	}
}

void loop()
{
	if (Serial.available()) {
		int cmd = Serial.read();
		switch (cmd) {
			case 'w':
				water_plants();
				break;
			case 'm':
				measure_moisture();
				break;
			default:
				Serial.print("unknown command: ");
				Serial.print((char) cmd);
				Serial.print("\r\nexpected [wm]\r\n");
				break;
		}
	}
}

void water_plants()
{
	Serial.print("WATER ON ");
	Serial.print(WATERTIME);
	Serial.print("\r\n");
	digitalWrite(MOTORPIN, HIGH);
	digitalWrite(LEDPIN, HIGH);
	delay(WATERTIME);
	digitalWrite(MOTORPIN, LOW);
	digitalWrite(LEDPIN, LOW);
	Serial.print("WATER OFF\r\n");
}

void measure_moisture()
{
	Serial.print("MEASURING\r\n");
	digitalWrite(PROBEPIN, HIGH); // Turn probe power on only when measuring.
	digitalWrite(LEDPIN, HIGH);
	delay(WARMUP); // Short delay to reach current steady-state.
	int m1 = analogRead(MOIST1);
	int m2 = analogRead(MOIST2);
	digitalWrite(PROBEPIN, LOW); // Turn probe off.
	digitalWrite(LEDPIN, LOW);

	Serial.print("m1: ");
	Serial.print(m1);
	Serial.print("\r\n");
	Serial.print("m2: ");
	Serial.print(m2);
	Serial.print("\r\n");
}
