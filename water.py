import serial
import time
import datetime

with serial.Serial('/dev/ttyACM0') as ser:
    print("connected to %s" % ser.name)
    time.sleep(2) # Wait for Arduino to reboot.
    print('Watering start %s' % datetime.datetime.now().ctime())
    ser.write(b'w')
    ser.flush()
    while True:
        line = ser.readline()
        print(line)
        if line.startswith('WATER OFF'):
            break
    print('Watering done %s' % datetime.datetime.now().ctime())

